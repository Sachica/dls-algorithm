/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import colecciones_seed.GrafoND;
import colecciones_seed.ListaCD;
import colecciones_seed.Vertice;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import modelo.AristaPoint;
import modelo.DataGraph;
import modelo.GraphDLS;
import modelo.MyPoint;
import vista.GraphFrame;
import vista.Task;

/**
 *
 * @author Sachikia
 */
public class GraphController extends Observable implements ActionListener, Observer, ChangeListener, MouseListener{
    private final GraphFrame graphFrame;
    private final GraphDLS<DataGraph<Integer>> dls;
    private final AristaPoint currentArista;
    private final Task currentTask;
    
    public GraphController() {
        this.dls = new GraphDLS<>();
        this.dls.addObserver(this);
        this.graphFrame = new GraphFrame(this, this, this);
        this.currentArista = new AristaPoint();
        this.currentTask = new Task(this.graphFrame.getPanelDraw());
        this.dls.addObserver(this.currentTask);
        
        //Test
        this.test();
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if(ae.getSource() == this.graphFrame.getBtnUnion()){
            try{
                int info1 = Integer.parseInt(this.graphFrame.getInfo1()), info2 = Integer.parseInt(this.graphFrame.getInfo2());
                this.dls.getGraph().insertarArista(new DataGraph(null, info1), new DataGraph(null, info2));
                this.graphFrame.getPanelDraw().getGraph().insertarArista(new DataGraph(null, info1), new DataGraph(null, info2));
                this.graphFrame.getPanelDraw().repaint();
            }catch(NumberFormatException err){
            }
        }
        
        if(ae.getSource() == this.graphFrame.getBtnUpd()){
            try{
                int rootVal = Integer.parseInt(this.graphFrame.getRoot());
                DataGraph<Integer> root = new DataGraph(null, rootVal);
                this.dls.searchRoot(root);
                this.graphFrame.getPanelDraw().searchRoot(root);
            }catch(NumberFormatException err){
            }
        }
        
        if(ae.getSource() == this.graphFrame.getBtnRun()){
            try{
                if(!this.currentTask.isActivo() && !this.dls.getGraph().esVacio()){
                    DataGraph<Integer> data = new DataGraph<>(null, this.graphFrame.getTarget());
                    this.dls.setData(data, this.graphFrame.getLimite());
                    this.currentTask.setActivo(true);
                    new Thread(this.dls).start();
                    this.graphFrame.setEnables(false);
                }
            }catch(NumberFormatException err){
                System.out.println("Dato invalido");
            }
        }
        
        if(ae.getSource() == this.graphFrame.getBtnRes()){
            this.currentTask.setActivo(false);
            this.dls.resetEstados();
            this.graphFrame.getPanelDraw().repaint();
        }
        
        if(ae.getSource() == this.graphFrame.getBtnDel()){
            this.currentTask.setActivo(false);
            this.dls.reset();
            this.graphFrame.getPanelDraw().reset();
            this.graphFrame.getPanelDraw().repaint();
        }
        
        if(ae.getSource() == this.graphFrame.getBtnBack()){
            setChanged();
            notifyObservers(this);
        }
    }

    @Override
    public void update(Observable o, Object o1) {
        if(o1 instanceof Boolean){
            this.currentTask.setActivo(false);
            this.graphFrame.setEnables(true);
        }
    }

    @Override
    public void stateChanged(ChangeEvent ce) {
        if(ce.getSource() == this.graphFrame.getSpinner() && this.graphFrame.getPanelDraw().getRoot()!=null){
            int val = this.graphFrame.getLimite();
            this.dls.setLimite(val);
            this.graphFrame.getPanelDraw().setLevel(val);
        }
        
        if(ce.getSource() == this.graphFrame.getSlider()){
            this.dls.setTime(this.graphFrame.getSlider().getValue());
        }
    }
    
    @Override
    public void mouseClicked(MouseEvent me) {
        if(me.getButton() == MouseEvent.BUTTON1){
            try{
                int r = Integer.parseInt(JOptionPane.showInputDialog(this.graphFrame, "Number:").trim());
                ListaCD<Vertice> list = this.dls.getGraph().getVertices();
                boolean can = true;
                for(Vertice c : list){
                    if(((DataGraph)c.getInfo()).getCollision(new MyPoint(me.getPoint()))) {
                        can = false;
                        break;
                    }
                }
                if(can){
                    DataGraph data = new DataGraph(new MyPoint(me.getPoint()), r);
                    this.dls.getGraph().insertarVertice(data);
                    this.graphFrame.getPanelDraw().getGraph().insertarVertice(data);
                    this.graphFrame.getPanelDraw().repaint();
                }
            }catch(NumberFormatException err){
            }
        }
        
        if(me.getButton() == MouseEvent.BUTTON3){
            if(this.currentArista.getP1()==null){
                this.currentArista.setP1(new MyPoint(me.getPoint()));
            }else{
                this.currentArista.setP2(new MyPoint(me.getPoint()));
                if(this.currentArista.isFar()){
                    DataGraph info1 = null, info2 = null;
                    boolean can = false;
                    ListaCD<Vertice> vertices = this.dls.getGraph().getVertices();
                    for(Vertice v: vertices){
                        DataGraph d = (DataGraph)v.getInfo();
                        if(this.currentArista.getP1() != null && d.isInside(this.currentArista.getP1())){
                            info1 = d;
                            this.currentArista.setP1(null);
                        }else if(this.currentArista.getP2() != null && d.isInside(this.currentArista.getP2())){
                            info2 = d;
                            this.currentArista.setP1(null);
                            can = true;
                            break;
                        }
                    }
                    if(can){
                        this.dls.getGraph().insertarArista(info1, info2);
                        this.graphFrame.getPanelDraw().getGraph().insertarArista(info1, info2);
                        this.graphFrame.getPanelDraw().repaint();
                    }
                }
                this.currentArista.reset();
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent me) {
    }

    @Override
    public void mouseReleased(MouseEvent me) {
    }

    @Override
    public void mouseEntered(MouseEvent me) {
    }

    @Override
    public void mouseExited(MouseEvent me) {
    }

    public void initTest(){
        DataGraph<Integer> data[] = new DataGraph[20];
        int x = 520, y = 580, p = 40, xi = -1;
        for(int i=0; i<data.length; i++){
            data[i] = new DataGraph<>(new MyPoint(x+(p*xi), y-p), i+1);
            this.dls.getGraph().insertarVertice(data[i]);
            this.graphFrame.getPanelDraw().getGraph().insertarVertice(data[i]);
            if(i>0) {
                this.dls.getGraph().insertarArista(data[i-1], data[i]);
                this.graphFrame.getPanelDraw().getGraph().insertarArista(data[i-1], data[i]);
            }
            xi *= -1;
            if(i%2 == 0) {
                p += 50;
            }
        }
        this.graphFrame.getPanelDraw().repaint();
    }
  
    private void test(){
        DataGraph<Integer> data[] = new DataGraph[20];
        Integer arista[][] = {{0,1},{1,2},{2,3},{2,4},{3,5},{5,6},{6,7},
                              {4,8},{8,10},{4,11},{8,9},{13,10},{13,12},{13,14},
                              {14,15},{15,16},{16,17},{14,18},{18,19}};
        /*
        java.awt.Point[164,47]      0=Oradea
        java.awt.Point[121,110]     1=Zerind
        java.awt.Point[88,170]      2=Arad
        java.awt.Point[93,300]      3=Timisoara
        java.awt.Point[317,225]     4=Sibiu
        java.awt.Point[231,350]-    5=Lugoj
        java.awt.Point[237,412]-    6=Mehadia
        java.awt.Point[231,473]-    7=Dobreta
        java.awt.Point[365,302]-    8=Rimnicu Velcea
        java.awt.Point[405,493]-    9=Craiova
        java.awt.Point[540,367]-    10=Pitesti
        java.awt.Point[511,238]-    11=Fagaras
        java.awt.Point[646,520]-    12=Giurgiu
        java.awt.Point[694,430]-    13=Bucharest
        java.awt.Point[807,395]-    14=Orziceni
        java.awt.Point[909,246]-    15=Vasliu
        java.awt.Point[839,149]-    16=Iasi
        java.awt.Point[706,102]-    17=Neamt
        java.awt.Point[957,395]-    18=Hirsova
        java.awt.Point[1015,484]-   19=Eforie
        */
        data[0] = new DataGraph<>(new MyPoint(164, 47), 0);
        data[1] = new DataGraph<>(new MyPoint(121,110), 1);
        data[2] = new DataGraph<>(new MyPoint(88,170), 2);
        data[3] = new DataGraph<>(new MyPoint(93,300), 3);
        data[4] = new DataGraph<>(new MyPoint(317,225), 4);
        data[5] = new DataGraph<>(new MyPoint(231,350), 5);
        data[6] = new DataGraph<>(new MyPoint(237,412), 6);
        data[7] = new DataGraph<>(new MyPoint(231,473), 7);
        data[8] = new DataGraph<>(new MyPoint(365,302), 8);
        data[9] = new DataGraph<>(new MyPoint(405,493), 9);
        data[10] = new DataGraph<>(new MyPoint(540,367), 10);
        data[11] = new DataGraph<>(new MyPoint(511,238), 11);
        data[12] = new DataGraph<>(new MyPoint(646,520), 12);
        data[13] = new DataGraph<>(new MyPoint(694,430), 13);
        data[14] = new DataGraph<>(new MyPoint(807,395), 14);
        data[15] = new DataGraph<>(new MyPoint(909,246), 15);
        data[16] = new DataGraph<>(new MyPoint(839,149), 16);
        data[17] = new DataGraph<>(new MyPoint(706,102), 17);
        data[18] = new DataGraph<>(new MyPoint(957,395), 18);
        data[19] = new DataGraph<>(new MyPoint(1015,484), 19);
        
        for(DataGraph datag: data){
            this.dls.getGraph().insertarVertice(datag);
            this.graphFrame.getPanelDraw().getGraph().insertarVertice(datag);            
        }
        
        for(Integer ari[]: arista){
            this.dls.getGraph().insertarArista(data[ari[1]], data[ari[0]]);
            this.graphFrame.getPanelDraw().getGraph().insertarArista(data[ari[1]], data[ari[0]]);
        }
        this.graphFrame.getPanelDraw().repaint();
    }
    
    public GraphFrame getGraphFrame() {
        return graphFrame;
    }
}
