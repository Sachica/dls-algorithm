/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import vista.MainFrame;

/**
 *
 * @author Sachikia
 */
public class MainController implements ActionListener, Observer{
    private TreeController treeController;
    private GraphController graphController;
    private final MainFrame mainFrame;

    public MainController() {
        this.mainFrame = new MainFrame(this);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if(ae.getSource() == this.mainFrame.getBtnTree()){
            this.treeController = new TreeController();
            this.treeController.addObserver(this);
        }
        
        if(ae.getSource() == this.mainFrame.getBtnGraph()){
            this.graphController = new GraphController();
            this.graphController.addObserver(this);
        }
        this.mainFrame.setVisible(false);
    }
    
    @Override
    public void update(Observable o, Object o1) {
        if(o1 instanceof TreeController){
            ((TreeController)o1).getMainFrame().dispose();
            o1 = null;
        }else if(o1 instanceof GraphController){
            ((GraphController)o1).getGraphFrame().dispose();
            o1 = null;
        }
        this.mainFrame.setVisible(true);
    }
    
    public static void main(String[] args) {
        MainController c = new MainController();
    }
}
