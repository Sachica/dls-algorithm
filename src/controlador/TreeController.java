/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import modelo.TreeDLS;
import vista.TreeFrame;
import vista.Task;

/**
 *
 * @author Sachikia
 */
public class TreeController extends Observable implements ActionListener, Observer, ChangeListener{
    protected TreeFrame frame;
    protected TreeDLS<Integer> dls;
    protected  Task currentTask;
    
    public TreeController() {
        super();
        this.dls = new TreeDLS<>();
        this.frame = new TreeFrame(this.dls.getTree(), this, this);
        this.currentTask = new Task(this.frame.getPanelDraw());
        
        this.dls.addObserver(this.currentTask);
        this.dls.addObserver(this);
        
        //Puede omitirse, solo es para testear
        this.initTest();
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if(ae.getSource() == ((TreeFrame)this.frame).getBtnIns()){
            try{
                this.dls.getTree().insertar(((TreeFrame)this.frame).getData());
                this.frame.getPanelDraw().repaint();
            }catch(NumberFormatException err){
                System.out.println("Dato invalido");
            }
        }
        
        if(ae.getSource() == this.frame.getBtnDel()){
            this.currentTask.setActivo(false);
            this.dls.getTree().setRaiz(null);
            this.frame.getPanelDraw().repaint();
        }
        
        if(ae.getSource() == this.frame.getBtnRun()){
            try{
                if(!this.currentTask.isActivo() && !this.dls.getTree().esVacio()){
                    this.dls.setData(this.frame.getTarget(), this.frame.getLimite());
                    this.currentTask.setActivo(true);
                    new Thread(this.dls).start();
                    this.frame.setEnables(false);
                }
            }catch(NumberFormatException err){
                System.out.println("Dato invalido");
            }
        }
        
        if(ae.getSource() == this.frame.getBtnRes()){
            this.currentTask.setActivo(false);
            this.dls.resetEstados();
            this.frame.getPanelDraw().repaint();
        }

        if(ae.getSource() == this.frame.getBtnBack()){
            setChanged();
            notifyObservers(this);
        }
    }
    
    @Override
    public void update(Observable o, Object o1) {
        if(o1 != null){
            this.frame.setEnables(true);
            this.currentTask.setActivo(false);
        }
    }

    @Override
    public void stateChanged(ChangeEvent ce) {
        if(ce.getSource() == this.frame.getSpinner()){
            this.frame.getPanelDraw().setLevel(Integer.parseInt(this.frame.getSpinner().getValue().toString()));
        }
        
        if(ce.getSource() == this.frame.getSlider()){
            this.dls.setTime(this.frame.getSlider().getValue());
        }
    }

    public void initTest(){
        this.dls.getTree().insertar(60);
        this.dls.getTree().insertar(30);
        this.dls.getTree().insertar(20);
        this.dls.getTree().insertar(10);
        this.dls.getTree().insertar(35);
        this.dls.getTree().insertar(25);
        this.dls.getTree().insertar(15);
        this.dls.getTree().insertar(90);
        this.dls.getTree().insertar(95);
        this.dls.getTree().insertar(70);
        this.dls.getTree().insertar(65);
        this.dls.getTree().insertar(80);
        this.dls.getTree().insertar(85);
        this.frame.getPanelDraw().repaint();
    }

    public TreeFrame getMainFrame() {
        return frame;
    }
}
