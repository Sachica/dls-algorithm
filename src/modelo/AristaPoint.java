/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import vista.HDraw;

/**
 *
 * @author Sachikia
 */
public class AristaPoint {
    private MyPoint p1;
    private MyPoint p2;

    public AristaPoint() {
    }

    public AristaPoint(MyPoint p1, MyPoint p2) {
        this.p1 = p1;
        this.p2 = p2;
    }
    
    public boolean isFar(){
        return Math.sqrt(Math.pow(this.p2.x-this.p1.x, 2)+Math.pow(this.p2.y-this.p1.y, 2)) >= HDraw.getDIAMETER()/2;
    }

    public MyPoint getP1() {
        return p1;
    }

    public void setP1(MyPoint p1) {
        this.p1 = p1;
    }

    public MyPoint getP2() {
        return p2;
    }

    public void setP2(MyPoint p2) {
        this.p2 = p2;
    }
    
    public void reset(){
        this.p1 = null;
        this.p2 = null;
    }
}
