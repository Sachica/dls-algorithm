/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;
import java.util.Observable;

/**
 *
 * @author Sachikia
 */
public class DLS<T> extends Observable implements Runnable{
    protected int time;
    protected T info;
    protected int limite;

    public DLS() {
        this.time = 250; //Por defecto
    }
    
    @Override
    public void run() {
        try{
            boolean value = this.depthLimitedSearch(info, limite);
            setChanged();
            notifyObservers(value);
        }catch(InterruptedException err){
        }
    }
    
    protected boolean depthLimitedSearch(T info, int limite) throws InterruptedException{
        return false;
    }
    
    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public void setData(T info, int limite){
        this.setInfo(info);
        this.setLimite(limite);
    }
    
    public void setInfo(T info) {
        this.info = info;
    }

    public void setLimite(int limite) {
        this.limite = limite;
    }
}
