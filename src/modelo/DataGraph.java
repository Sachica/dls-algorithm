/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import vista.HDraw;

/**
 *
 * @author Sachikia
 */
public class DataGraph<T>{
    private MyPoint point;
    private T info;
    private Boolean state;

    public DataGraph() {
    }

    public DataGraph(MyPoint point, T info) {
        this.point = point;
        this.info = info;
        this.state = null;
    }

    public T getInfo() {
        return info;
    }

    public MyPoint getPoint() {
        return point;
    }

    public void setPoint(MyPoint point) {
        this.point = point;
    }

    public void setInfo(T info) {
        this.info = info;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null) return false;
        return ((DataGraph<T>)o).getInfo().equals(this.info);
    }
    
    public boolean getCollision(MyPoint other){
        return Math.sqrt(Math.pow(other.x-this.point.x, 2)+Math.pow(other.y-this.point.y, 2)) <= HDraw.getDIAMETER();
    }
    
    public boolean isInside(MyPoint other){
        return Math.sqrt(Math.pow(other.x-this.point.x, 2)+Math.pow(other.y-this.point.y, 2)) <= HDraw.getDIAMETER()/2;
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }
}
