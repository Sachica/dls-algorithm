/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import colecciones_seed.GrafoND;
import colecciones_seed.ListaCD;
import colecciones_seed.Vertice;

/**
 *
 * @author Sachikia
 */
public class GraphDLS<T> extends DLS<T>{
    private GrafoND<T> graph;
    private Vertice<T> root;
    
    public GraphDLS(){
        this.graph = new GrafoND<>();
        this.root = null;
    }
    
    @Override
    protected boolean depthLimitedSearch(T info, int limite) throws InterruptedException{
        boolean val = recursiveDLS(this.root, info, limite);
        this.resetEstados();
        return val;
    }

    protected boolean recursiveDLS(Vertice<T> root, T info, int limite) throws InterruptedException{
        if(root == null) return false;
        
        DataGraph data = ((DataGraph)root.getInfo());
        if(root.getInfo().equals(info)){
            this.update(data, Boolean.TRUE);
            return true;
        }
        
        this.update(data, Boolean.FALSE);
        root.setVisit(true);
        
        if(limite == 0) return false;
        
        ListaCD<Vertice> list = root.getVecinos();
        boolean val = false;
        for(Vertice v: list){
            if(!v.getVisit()){
                val = recursiveDLS(v, info, limite - 1);
                if(val) break;
            }
        }
        
        return val;
    }

    private void update(DataGraph data, Boolean state) throws InterruptedException{
        data.setState(state);
        setChanged();
        notifyObservers();
        Thread.sleep(this.time);
    }
    
    public GrafoND<T> getGraph() {
        return graph;
    }

    public Vertice<T> getRoot() {
        return root;
    }

    public void setRoot(Vertice<T> root) {
        this.root = root;
    }
    
    public void searchRoot(T info){
        ListaCD<Vertice> list = this.graph.getVertices();
        for(Vertice v : list){
            if(v.getInfo().equals(info)){
                this.root = v;
                break;
            }
        }
    }
    
    public void resetEstados(){
        ListaCD<Vertice> list = this.graph.getVertices();
        for(Vertice v : list){
            v.setVisit(false);
            ((DataGraph)v.getInfo()).setState(null);
        }
    }
    
    public void reset(){
        this.graph = new GrafoND<>();
        this.root = null;
    }
}
