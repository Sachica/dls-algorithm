/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.awt.Point;
import vista.HDraw;

/**
 *
 * @author Sachikia
 */
public class MyPoint extends Point{

    public MyPoint() {
    }

    public MyPoint(Point point) {
        super(point);
        this.x -= HDraw.getDIAMETER()/2;
        this.y -= HDraw.getDIAMETER()/2;
    }

    public MyPoint(int i, int i1) {
        super(i, i1);
        this.x -= HDraw.getDIAMETER()/2;
        this.y -= HDraw.getDIAMETER()/2;
    }
}
