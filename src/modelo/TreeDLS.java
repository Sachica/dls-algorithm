/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import colecciones_seed.ArbolBinarioBusqueda;
import colecciones_seed.NodoBin;

/**
 *
 * @author Sachikia
 */
public class TreeDLS<T> extends DLS<T>{
    private final ArbolBinarioBusqueda<T> tree;

    public TreeDLS() {
        super();
        this.tree = new ArbolBinarioBusqueda<>();
    }
    
    @Override
    protected boolean depthLimitedSearch(T info, int limite) throws InterruptedException{
        return this.recursiveDLS(this.tree.getRaiz(), info, limite);
    }

    protected boolean recursiveDLS(NodoBin<T> root, T info, int limite) throws InterruptedException{
        if(root == null) return false;

        if(root.getInfo() == info) {
            this.update(root, Boolean.TRUE);
            return true;
        }
        
        this.update(root, Boolean.FALSE);
        if(limite == 0) {
            return false;
        }
        
        boolean result = recursiveDLS(root.getIzq(), info, limite - 1);
        if(!result) {
            return recursiveDLS(root.getDer(), info, limite - 1);
        }
        return result;
    }

    protected void update(NodoBin<T> root, Boolean estado) throws InterruptedException{
        root.setEstado(estado);
        setChanged();
        notifyObservers();
        Thread.sleep(this.time);
    }
    
    public void resetEstados(){
        this.reset(this.tree.getRaiz());
    }
    
    public void reset(NodoBin<T> root){
        if(root == null) return;
        root.setEstado(null);
        reset(root.getIzq());
        reset(root.getDer());
    }
    
    public ArbolBinarioBusqueda<T> getTree(){
        return this.tree;
    }
}
