/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.Insets;

/**
 *
 * @author Sachikia
 */
public class Utilidad {
    public static void insert(Container contenedor, Component componente, GridBagConstraints gr, int gridx, int gridy, double weightx, double weighty, int gridwidth, int gridheight, int fill, int anchor, Insets insets, int ipadx, int ipady){
        if(insets == null){
            insets = new Insets(0, 0, 0, 0);
        }
        gr.gridx = gridx;
        gr.gridy = gridy;
        gr.weightx = weightx;
        gr.weighty = weighty;
        gr.gridwidth = gridwidth;
        gr.gridheight = gridheight;
        gr.fill = fill;
        gr.anchor = anchor;
        gr.insets = insets;
        gr.ipadx = ipadx;
        gr.ipady = ipady;
        contenedor.add(componente, gr);
        reset(gr);
    }
    
    public static void reset(GridBagConstraints gr){
        gr.anchor = GridBagConstraints.CENTER;
        gr.fill = GridBagConstraints.NONE;
        gr.gridheight = 1;
        gr.gridwidth = 1;
        gr.insets.bottom = 0;
        gr.insets.top = 0;
        gr.insets.left = 0;
        gr.insets.right = 0;
        gr.weightx = 0;
        gr.weighty = 0;
        gr.ipadx = 0;
        gr.ipady = 0;
    }
}
