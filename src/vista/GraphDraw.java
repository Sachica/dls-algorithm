/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import colecciones_seed.Arista;
import colecciones_seed.GrafoND;
import colecciones_seed.ListaCD;
import colecciones_seed.Vertice;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.io.File;
import javax.swing.ImageIcon;
import modelo.DataGraph;

/**
 *
 * @author Sachikia
 */
public class GraphDraw<T> extends HDraw{
    private GrafoND<DataGraph<T>> graph;
    private final ImageIcon IMG = new ImageIcon(new File(".").getAbsolutePath()+"\\res\\mapa2.png");
    private Vertice root;
    
    public GraphDraw() {
        super();
        this.graph = new GrafoND<>();
    }
    
    @Override
    public void paintComponent(Graphics g){
        Graphics2D g2 =(Graphics2D)g;
        super.paintComponent(g2);
        g.drawImage(this.IMG.getImage(), 0, 0, (int)this.getWidth(), (int)this.getHeight(), null);
        if(this.graph != null && !this.graph.esVacio()) {
            ListaCD<Arista> listA = this.graph.getAristas();
            for(Arista a : listA){
                DataGraph d1 = ((DataGraph)a.getVertA().getInfo()), d2 = ((DataGraph)a.getVertB().getInfo());
                g2.drawLine(d1.getPoint().x+(HDraw.getDIAMETER()/2), d1.getPoint().y+(HDraw.getDIAMETER()/2), d2.getPoint().x+(HDraw.getDIAMETER()/2), d2.getPoint().y+(HDraw.getDIAMETER()/2));
            }
            
            
            ListaCD<Vertice> listV = this.graph.getVertices();
            for(Vertice v : listV){
                DataGraph<Integer> data = (DataGraph<Integer>)v.getInfo();
                if(data.getState() == null) {
                    g2.setColor(Color.WHITE);
                }else if(data.getState()){
                    g2.setColor(Color.GREEN);
                }else if(!data.getState()){
                    g2.setColor(Color.YELLOW);
                }
                g2.fillOval(data.getPoint().x, data.getPoint().y, DIAMETER, DIAMETER);
                g2.setColor(Color.BLACK);
                g2.drawString(data.getInfo().toString(), data.getPoint().x+10, data.getPoint().y+20);
            }                
            
            if(this.root != null){
                this.recursivePaint(this.root, g2, this.level+1);
                this.resetVisit();
            }
        }
    }

    private void recursivePaint(Vertice root, Graphics2D g, int levelr) {
        if(root == null || levelr == 0 || root.getVisit()) return;
        
        root.setVisit(true);
        DataGraph data = (DataGraph)root.getInfo();
        g.setColor(Color.RED);
        g.drawOval(data.getPoint().x-5, data.getPoint().y-5, DIAMETER+10, DIAMETER+10);
        g.drawString(""+(this.level-levelr+1), data.getPoint().x-8, data.getPoint().y+4);
        g.setColor(Color.BLACK);
        ListaCD<Vertice> vecinos = root.getVecinos();
        for(Vertice v : vecinos){
            if(!v.getVisit()){
                this.recursivePaint(v, g, levelr-1);
            }
        }
    }
    
    public GrafoND<DataGraph<T>> getGraph() {
        return graph;
    }

    public Vertice getRoot() {
        return root;
    }
    
    public void searchRoot(DataGraph info) {
        ListaCD<Vertice> list = this.graph.getVertices();
        for(Vertice v : list){
            if(v.getInfo().equals(info)){
                this.root = v;
                break;
            }
        }
        this.repaint();
    }
    
    public void reset(){
        this.graph = new GrafoND<>();
        this.root = null;
    }

    private void resetVisit() {
        this.root.setVisit(false);
        ListaCD<Vertice> list = this.graph.getVertices();
        for(Vertice v : list){
            v.setVisit(false);
        }
    }
}
