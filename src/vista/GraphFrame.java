/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeListener;
import util.Utilidad;

/**
 *
 * @author Sachikia
 */
public class GraphFrame extends HFrame{
    private final GraphDraw panelDraw;
    
    private final JTextField txtRoot;
    private final JTextField txtInfo1;
    private final JTextField txtInfo2;
    private final JButton btnUnion;
    private final JButton btnUpd;

    public GraphFrame(ActionListener e, ChangeListener c, MouseListener m) {
        super(e, c);
        super.setTitle("Depth Limited Search - Graph");
        
        this.panelDraw = new GraphDraw();
        this.txtInfo1 = new JTextField();
        this.txtInfo2 = new JTextField();
        this.txtRoot = new JTextField();
        this.btnUnion = new JButton("Link");
        this.btnUpd = new JButton("Update");
        
        this.init(e, m);
    }

    private void init(ActionListener e, MouseListener m) {
        GridBagConstraints gr = new GridBagConstraints();
        
        this.panelDraw.addMouseListener(m);
        this.panelDraw.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Visual"));
        
        Utilidad.insert(this.panelSett, new JLabel("Root"), gr, 0, 0, 0, 0, 1, 1, GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER, new Insets(0, 5, 5, 0), 0, 0);
        Utilidad.insert(this.panelSett, this.txtRoot, gr, 1, 0, 0, 0, 1, 1, GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER, new Insets(0, 5, 5, 5), 60, 6);
        Utilidad.insert(this.panelSett, this.btnUpd, gr, 2, 0, 0, 0, 1, 1, GridBagConstraints.NONE, GridBagConstraints.CENTER, new Insets(0, 5, 5, 5), 0, 0);
        
        Utilidad.insert(this.panelSett, this.txtInfo1, gr, 4, 0, 0, 0, 1, 1, GridBagConstraints.NONE, GridBagConstraints.CENTER, new Insets(0, 5, 5, 0), 35, 6);
        Utilidad.insert(this.panelSett, this.txtInfo2, gr, 5, 0, 0, 0, 1, 1, GridBagConstraints.NONE, GridBagConstraints.CENTER, new Insets(0, 0, 5, 5), 35, 6);
        Utilidad.insert(this.panelSett, this.btnUnion, gr, 6, 0, 0, 0, 1, 1, GridBagConstraints.NONE, GridBagConstraints.CENTER, new Insets(0, 0, 5, 5), 0, 0);
        
        this.panelSett.remove(this.lblDelay);
        this.panelSett.remove(this.slider);
        this.slider.setOrientation(JSlider.HORIZONTAL);
        
        Utilidad.insert(this.panelSett, this.lblDelay, gr, 4, 1, 0, 0, 1, 1, GridBagConstraints.NONE, GridBagConstraints.CENTER, new Insets(0, 5, 5, 5), 0, 0);
        Utilidad.insert(this.panelSett, this.slider, gr, 5, 1, 0, 0, 2, 1, GridBagConstraints.NONE, GridBagConstraints.CENTER, new Insets(0, 0, 5, 0), -110, 0);
        
        Utilidad.insert(super.getContentPane(), this.panelDraw, gr, 0, 0, 1, 1, 2, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, null, 0, 0);
        
        this.btnUnion.addActionListener(e);
        this.btnUpd.addActionListener(e);
        
        this.spLevel.setModel(new SpinnerNumberModel(0, 0, 1000, 1));
        ((JSpinner.DefaultEditor) this.spLevel.getEditor()).getTextField().setEditable(false);
        ((JSpinner.DefaultEditor) this.spLevel.getEditor()).getTextField().setHorizontalAlignment(SwingConstants.CENTER);
        
        super.pack();
    }

    @Override
    public GraphDraw getPanelDraw() {
        return panelDraw;
    }

    public String getRoot() {
        return txtRoot.getText().trim();
    }

    public String getInfo1() {
        return txtInfo1.getText().trim();
    }

    public String getInfo2() {
        return txtInfo2.getText().trim();
    }

    public JButton getBtnUnion() {
        return btnUnion;
    }

    public JButton getBtnUpd() {
        return btnUpd;
    }
    
    @Override
    public void setEnables(boolean change){
        this.btnDel.setEnabled(change);
        this.btnRes.setEnabled(change);
        this.btnUnion.setEnabled(change);
        this.btnUpd.setEnabled(change);
    }
}
