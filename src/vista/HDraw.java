/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;
import javax.swing.JPanel;

/**
 *
 * @author Sachikia
 */
public class HDraw extends JPanel{
    protected static final int DIAMETER = 30;
    protected final int MY_HEIGHT = 20;
    protected int level;

    public HDraw() {
        super();
        this.level = 0;
    }
    
    public void setLevel(int level){
        this.level = level;
        this.repaint();
    }

    public static int getDIAMETER() {
        return DIAMETER;
    }
}
