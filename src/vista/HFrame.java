/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JSpinner.DefaultEditor;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeListener;
import util.Utilidad;

/**
 *
 * @author Sachikia
 */
public class HFrame extends JFrame{
    protected final Dimension size = new Dimension(1100, 750);
    
    protected final JPanel panelSett;
    protected final JTextField txtTar;
    protected final JLabel lblDelay;
    protected final JSlider slider;
    protected final JButton btnRes;
    protected final JButton btnDel;
    protected final JButton btnRun;
    protected final JButton btnBack;
    protected final JSpinner spLevel;

    public HFrame(ActionListener e, ChangeListener c) {
        super.setSize(size);
        super.setMinimumSize(size);
        super.getContentPane().setLayout(new GridBagLayout());
        
        this.panelSett = new JPanel(new GridBagLayout());
        this.spLevel = new JSpinner();
        
        this.slider = new JSlider(500, 2000);
        this.lblDelay = new JLabel("Delay");
        this.btnRes = new JButton("Restart");
        this.btnDel = new JButton("Clear");
        this.btnRun = new JButton("Run");
        this.btnBack = new JButton("Back");
        this.txtTar = new JTextField();
        
        this.init(e, c);
        
        super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        super.setLocationRelativeTo(null);
        super.setVisible(true);
    }
    
    private void init(ActionListener e, ChangeListener c){
        GridBagConstraints gr = new GridBagConstraints();
        JPanel aux = new JPanel(new GridBagLayout());
        aux.setBackground(Color.yellow);
        this.panelSett.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Settings"));
        
        Utilidad.insert(super.getContentPane(), this.panelSett, gr, 0, 1, 0, 0, 1, 1, GridBagConstraints.NONE, GridBagConstraints.CENTER, null, 10, 0);
        Utilidad.insert(super.getContentPane(), aux,            gr, 1, 1, 1, 0, 1, 1, GridBagConstraints.NONE, GridBagConstraints.LAST_LINE_END, new Insets(0, 0, 5, 5), 0, 0);
                
        Utilidad.insert(this.panelSett, new JLabel("Target"),   gr, 0, 1, 0, 0, 1, 1, GridBagConstraints.HORIZONTAL,    GridBagConstraints.CENTER, new Insets(0, 5, 0, 0), 0, 0);
        Utilidad.insert(this.panelSett, this.txtTar,            gr, 1, 1, 0, 0, 1, 1, GridBagConstraints.HORIZONTAL,    GridBagConstraints.CENTER, new Insets(0, 5, 5, 5) , 60, 6);
        
        Utilidad.insert(this.panelSett, new JLabel("Level"),    gr, 0, 2, 0, 0, 1, 1, GridBagConstraints.HORIZONTAL,    GridBagConstraints.CENTER, new Insets(0, 5, 0, 0), 0, 0);
        Utilidad.insert(this.panelSett, this.spLevel,           gr, 1, 2, 0, 0, 1, 1, GridBagConstraints.NONE,          GridBagConstraints.CENTER, new Insets(0, 5, 5, 5), 33, 8);
        
        Utilidad.insert(this.panelSett, this.btnRes,            gr, 0, 3, 0, 0, 1, 1, GridBagConstraints.HORIZONTAL,    GridBagConstraints.CENTER, new Insets(0, 5, 5, 0), 0, 0);
        Utilidad.insert(this.panelSett, this.btnDel,            gr, 1, 3, 0, 0, 1, 1, GridBagConstraints.HORIZONTAL,    GridBagConstraints.CENTER, new Insets(0, 5, 5, 5), 0, 0);
        Utilidad.insert(this.panelSett, this.btnRun,            gr, 2, 3, 0, 0, 1, 1, GridBagConstraints.HORIZONTAL,    GridBagConstraints.CENTER, new Insets(0, 0, 5, 5), 0, 0);
        
        Utilidad.insert(this.panelSett, this.lblDelay,          gr, 4, 0, 0, 0, 1, 1, GridBagConstraints.NONE,          GridBagConstraints.CENTER, null, 0, 0);
        Utilidad.insert(this.panelSett, this.slider,            gr, 4, 1, 0, 0, 1, 3, GridBagConstraints.NONE,          GridBagConstraints.CENTER, new Insets(0, 0, 5, 5), 0, -110);
        
        Utilidad.insert(aux, this.btnBack, gr, 0, 0, 0, 0, 1, 1, GridBagConstraints.NONE, GridBagConstraints.LAST_LINE_END, null, 0, 0);
        
        this.spLevel.setModel(new SpinnerNumberModel(0, 0, 6, 1));
        ((DefaultEditor) this.spLevel.getEditor()).getTextField().setEditable(false);
        ((DefaultEditor) this.spLevel.getEditor()).getTextField().setHorizontalAlignment(SwingConstants.CENTER);
        
        this.spLevel.addChangeListener(c);
        this.slider.addChangeListener(c);
        this.btnRes.addActionListener(e);
        this.btnDel.addActionListener(e);
        this.btnRun.addActionListener(e);
        this.btnBack.addActionListener(e);
        
        this.slider.setOrientation(JSlider.VERTICAL);
        
        super.pack();
    }
    
    public JButton getBtnRes() {
        return btnRes;
    }

    public JButton getBtnDel() {
        return btnDel;
    }

    public JButton getBtnRun() {
        return btnRun;
    }
    
    public JSpinner getSpinner(){
        return this.spLevel;
    }
    
    public Integer getLimite(){
        return Integer.parseInt(this.spLevel.getValue().toString()); 
    }
    
    public Integer getTarget()throws NumberFormatException{
        return Integer.parseInt(this.txtTar.getText().trim()); 
    }

    public JSlider getSlider() {
        return slider;
    }

    public JButton getBtnBack() {
        return btnBack;
    }
    
    public HDraw getPanelDraw(){
        return null;
    }
    
    public void setEnables(boolean change){
        this.btnDel.setEnabled(change);
        this.btnRes.setEnabled(change);
    }
}
