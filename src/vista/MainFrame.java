/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import util.Utilidad;

/**
 *
 * @author Sachikia
 */
public class MainFrame extends JFrame{
    private final JButton btnTree;
    private final JButton btnGraph;

    public MainFrame(ActionListener e) {
        super("Depth Limited Search");
        super.setSize(new Dimension(300, 200));
        super.setMinimumSize(new Dimension(300, 200));
        super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        this.btnTree = new JButton("Binary Tree Version");
        this.btnGraph = new JButton("Graph Version");
        
        this.init(e);
        
        super.setLocationRelativeTo(null);
        super.setVisible(true);
    }

    private void init(ActionListener e) {
        this.btnGraph.addActionListener(e);
        this.btnTree.addActionListener(e);
        super.getContentPane().setLayout(new GridBagLayout());
        
        GridBagConstraints gr = new GridBagConstraints();
        
        Utilidad.insert(super.getContentPane(), this.btnTree, gr, 0, 0, 1, 0, 1, 1, GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER, new Insets(0, 20, 10, 20), 0, 0);
        Utilidad.insert(super.getContentPane(), this.btnGraph, gr, 0, 1, 1, 0, 1, 1, GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER, new Insets(0, 20, 0, 20), 0, 0);
        
        super.pack();
    }

    public JButton getBtnTree() {
        return btnTree;
    }

    public JButton getBtnGraph() {
        return btnGraph;
    }
}
