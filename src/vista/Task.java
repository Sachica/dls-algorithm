/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.util.Observable;
import java.util.Observer;
import modelo.DLS;

/**
 *
 * @author Sachikia
 */
public class Task implements Observer{
    private final HDraw panelDraw;
    private boolean activo;

    public Task(HDraw panelDraw){
        this.panelDraw = panelDraw;
        this.activo = false;
    }
    
    @Override
    public void update(Observable o, Object o1) {
        if(o1 instanceof Boolean) return;
        
        if(this.activo){
            
            this.panelDraw.validate();
            this.panelDraw.repaint();
        }else{
            ((DLS)o).setTime(0);
        }
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
}
