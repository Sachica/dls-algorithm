/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import colecciones_seed.ArbolBinarioBusqueda;
import colecciones_seed.NodoBin;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

/**
 *
 * @author Sachikia
 */
public class TreeDraw extends HDraw{
    private final ArbolBinarioBusqueda tree;
    
    public TreeDraw(ArbolBinarioBusqueda tree){
        super();
        this.tree = tree;
    }
    
    @Override
    public void paintComponent(Graphics g){
        Graphics2D g2 =(Graphics2D)g;
        super.paintComponent(g2);
        this.drawLevel(g);
        if(this.tree.getRaiz()!=null) {
            g.setColor(Color.BLACK);
            AffineTransform old = g2.getTransform();
            g2.translate((int)super.getSize().getWidth()/2, MY_HEIGHT);
            recursivePaint(g2, this.tree.getRaiz(), (int)super.getSize().getWidth()/2);
            g2.setTransform(old);
        }
    }
    
    private void recursivePaint(Graphics2D g, NodoBin root, int x){
        AffineTransform old;
        x = x<0 ? x*-1 : x;
        
        if(root.getIzq() != null) {
            g.drawLine(0, DIAMETER/2, -(x/2), MY_HEIGHT*4);
            old = g.getTransform();
            g.translate(-x/2, MY_HEIGHT*4);            
            recursivePaint(g, root.getIzq(), x/2);
            g.setTransform(old);
        }
        
        if(root.getDer() != null) {
            g.drawLine(0, DIAMETER/2, (x/2), MY_HEIGHT*4);
            this.fillCircle(root, g);
            old = g.getTransform();
            g.translate(x/2, MY_HEIGHT*4);  
            recursivePaint(g, root.getDer(), x/2);
            g.setTransform(old);
        }      
        
        this.fillCircle(root, g);
        g.setColor(Color.BLACK);
        g.drawString(root.getInfo().toString(), (-DIAMETER/2)+14, 24);
    }

    private void drawLevel(Graphics g) {
        int h = MY_HEIGHT*4, i = 0, w = (int)super.getSize().getWidth();
        while(h <= super.getSize().getHeight()){
            g.setColor(Color.BLUE);
            g.drawString("Nivel "+(i++), w-45, h-6);
            g.setColor(level+1==i ? Color.RED : Color.GRAY);
            g.drawLine(0, h, w, h);
            h+=MY_HEIGHT*4;
        }
    }

    private void fillCircle(NodoBin root, Graphics2D g){
        if(root.getEstado() != null && root.getEstado()) {
            g.setColor(Color.GREEN);
        }
        else if(root.getEstado() != null) {
            g.setColor(Color.YELLOW);
        }else{
            g.setColor(Color.WHITE);
        }
        
        g.fillOval(-DIAMETER/2, 0, DIAMETER, DIAMETER);
        g.setColor(Color.BLACK);
    }
    
    public ArbolBinarioBusqueda getTree() {
        return tree;
    }
}
