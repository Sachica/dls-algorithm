/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import colecciones_seed.ArbolBinarioBusqueda;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.event.ChangeListener;
import util.Utilidad;

/**
 *
 * @author Sachikia
 */
public class TreeFrame extends HFrame{
    private final TreeDraw panelDraw;
    
    private final JTextField txtData;
    private final JButton btnIns;
    
    public TreeFrame(ArbolBinarioBusqueda tree, ActionListener e, ChangeListener c) {
        super(e, c);
        super.setTitle("Depth Limited Search - Binary Tree Search");
        
        this.panelDraw = new TreeDraw(tree);

        this.btnIns = new JButton("Insert");
        this.txtData = new JTextField();
        
        this.init(e);
    }
    
    private void init(ActionListener e){
        GridBagConstraints gr = new GridBagConstraints();
        
        Utilidad.insert(super.getContentPane(), this.panelDraw, gr, 0, 0, 1, 1, 2, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, null, 0, 0);
        
        Utilidad.insert(this.panelSett, new JLabel("Value"),    gr, 0, 0, 0, 0, 1, 1, GridBagConstraints.HORIZONTAL,    GridBagConstraints.CENTER, new Insets(0, 5, 0, 0), 0, 0);
        Utilidad.insert(this.panelSett, this.txtData,           gr, 1, 0, 0, 0, 1, 1, GridBagConstraints.HORIZONTAL,    GridBagConstraints.CENTER, new Insets(0, 5, 5, 5), 60, 6);
        Utilidad.insert(this.panelSett, this.btnIns,            gr, 2, 0, 0, 0, 1, 1, GridBagConstraints.HORIZONTAL,    GridBagConstraints.CENTER, new Insets(0, 0, 5, 5), 10, 0);
        
        this.btnIns.addActionListener(e);
        
        super.pack();
    }
    
    @Override
    public TreeDraw getPanelDraw(){
        return this.panelDraw;
    }
    
    public JButton getBtnIns(){
        return this.btnIns;
    }

    public Integer getData()throws NumberFormatException{
        return Integer.parseInt(this.txtData.getText().trim()); 
    }
}
